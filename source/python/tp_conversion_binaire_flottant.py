def binaire(x,nb_bits=16):
    """
    :paramètres:
    - x est un nombre à vigule en décimal
    - nb_bits désigne le nombre de bits utilisés pour encoder après la virgule
    :return:
    la fonction renvoie l'écriture binaire du nombre à virgule décimal x
    """
    n = int(x)
    d=x-n
    nombre = ''
    rang = 0
    if n == 0:
        nombre = '0' + nombre
    while n > 0:
        r = n%2
        n = n//2
        nombre = str(r) + nombre
    nombre += '.'
    while d!= 0 and rang < nb_bits:
        rang +=1
        d=d*2
        if d >= 1:
            nombre += '1'
            d=d-1
        else:
            nombre += '0'
    return nombre

def decimal(n,nb_bits=16):
    """
    :paramètres:
    - n est un nombre à vigule en binaire
    - nb_bits désigne le nombre de bits utilisés pour encoder après la virgule
    :return:
    la fonction renvoie l'écriture décimale du nombre à virgule binaire n
    """
    a,b = n.split('.')
    nombre = 0
    for i in range(len(a)):
        nombre += int(a[i])*2**(len(a)-i-1)
    for i in range(len(b)):
        nombre += int(b[i])*2**-(i+1)
    #print(f"{nombre:.{nb_bits}f}")
    return nombre

if __name__ == '__main__':
    n = binaire(0.1,8)
    print(n)
    n = decimal(n)
    print(n)
    n = binaire(0.1,32)
    print(n)
    n = decimal(n)
    print(n)
