Les nombres à virgule
=====================

Représentation binaire
------------------------

Les nombres à virgule ont une représentation en binaire. Ils s'écrivent avec un point à la place de la virgule et se décomposent :

-   avant le point avec des puissances de 2 d'exposants poitifs;
-   après le point avec des puissance de 2 d'exposants négatifs.

La représentation dans certains cas est appoximative. La partie située après la virgule n'est pas nécessairement décomposable avec des puissances de 2 d'exposants négatifs.

.. admonition:: Exemple

    La partie après la virgule du nombre :math:`0,3` est représenté sur 1 octet par ``01001100``. 
    En effectuant le calcul des puissances de 2, on obtient:

    .. math::

        01001100_{2} &=  0 \times 2^{-1} + 1 \times 2^{-2} + 0 \times 2^{-3} + 0 \times 2^{-4} + 1 \times 2^{-5} + 1 \times 2^{-6} + 0 \times 2^{-7} + 0 \times 2^{-8}\\
        01001100_{2} &= 2^{-2} + 2^{-5} + 2^{-6}\\
        01001100_{2} &=  \dfrac{1}{4} + \dfrac{1}{32} + \dfrac{1}{64}\\
        01001100_{2} &= 0,296875 

Représentation en virgule fixe
------------------------------

On alloue un nombre de bits pour la partie entière située avant la virgule et un nombre de bits pour la partie située après la virgule. On dit que le nombre est représenté en **virgule fixe**.

.. admonition:: Exemple

    Le nombre :math:`1001,0100` est représenté en binaire avec virgule fixe. On alloue 4 bits à la partie avant la virgule et 4 bits pour celle après la virgule.
    
    Pour le convertir en écriture décimale, on associe à chaque chiffre sa puissance de 2.

    .. math::
        
        1001,01_{2} &= 1\times 2^{3} + 0\times 2^{2} + 0\times 2^{1} + 1\times 2^{0} + 0\times 2^{-1} + 1\times 2^{-2}\\
        1001,01_{2} &= 8 + 1 + \dfrac{1}{4}\\
        1001,01_{2} &= 9,25_{10}


Représentation en virgule flottante
-----------------------------------

La représentation d'un nombre à virgule avec une virgule fixe limite le nombre de représentations binaires. Pour augmenter le nombre de représentations binaires, on utilise une virgule flottante qui reprend le principe de l'écriture scientifique en utilisant une expression de la forme :math:`a \times 2^{n}` où :math:`a` est compris entre 1 et 2.

L'exposant :math:`n` correspond au décalage de la virgule dans le nombre en virgule fixe jusqu'au premier chiffre non nul. Voilà pourquoi on parle de nombre représenté en **virgule flottante**.

.. admonition:: Exemple

	-   :math:`11010101_{2}=1,1010101 \times 2^{7}`
	-   :math:`11010,101_{2}=1,1010101 \times 2^{3}`
	-   :math:`0,0000101_{2}=1,01 \times 2^{-5}`

Norme IEEE 754
--------------

L'écriture d'un nombre avec virgule flottante est de la forme 

.. math::
    
    \pm~m \times 2^{e}

Cette forme d'écriture permet d'encoder les nombres à virgule en suivant une **norme** mise au point par l'IEEE (Institute of Electrical and Electronics Engineers) .

La **norme** est différente selon les modèles d’architecture 32 ou 64 bits des ordinateurs. La représentation binaire se décompose en 3 parties : le **signe**, l'**exposant** et la **mantisse**.

Dans une architecture 32 bits on a:

-	le premier bit réservé au **signe** du nombre; le bit 0 pour un nombre positif et le bit 1 pour un nombre négatif.

-	les 8 bits suivants réservés à l'**exposant**. L'exposant est un nombre négatif ou positif mais il n'est pas encodé comme un nombre signé. On applique un **biais** qui vaut 127, c'est à dire qu'on ajoute 127 à la valeur de l'exposant.

-	les 23 derniers bits représentent la **mantisse** du nombre, c'est à dire les chiffres écrits après la virgule.

.. admonition:: Exemple
    
    On donne la représentation binaire d'un nombre flottant sur une architecture 32 bits:

    .. figure:: ../img/flottant_2.svg
        :align: center
        :width: 480
	
        Représentation d'un nombre flottant encodé en 32 bits
    
    -   le bit de signe vaut 1, donc le nombre est négatif.
    -   l'exposant ``e`` vaut :math:`10000110`.
    
        On calcule la valeur décimale de l'exposant en retirant le biais :math:`127`:

        .. math::

            e &= 2^{7}+2^{2}+2^{1} - 127\\
            e &= 128+4+2 - 127\\
            e &= 7
	
    -	la mantisse ``m`` vaut ``101011011``. On remet le bit ``1`` devant la virgule, on obtient ``1,101011011``. Si on décale la virgule de 7 rangs, on obtient le nombre à virgule fixe ``11010110,11``. On peut convertir la partie avant la virgule:
	
        .. math::
        
            11010110_{2} &= 2^{7} + 2^{6} + 2^{4} + 2^{2} + 2^{1}\\
            11010110_{2} &= 128 + 64 + 16 + 4 + 2\\
            11010110_{2} &= 214
                
        et celle après la virgule:

        .. math::

            0,11_{2} &= 2^{-1} + 2^{-2}\\
            0,11_{2} &= \dfrac{1}{2} + \dfrac{1}{4}\\
            0,11_{2} &= 0,75
	
    Au final, le nombre encodé en 32 bits a pour valeur décimale :math:`-214,75`.





