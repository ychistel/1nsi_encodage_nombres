Exercices
=========

.. exercice::

	On s'interesse aux nombres signés encodés sur 3 bits.

	#.	Combien de nombres distincts peut-on écrire sur 3 bits ?
	#.	Quels sont les nombres négatifs ? Quels sont les nombres positifs ?
	#.	Écrire chaque nombre en binaire et donner sa valeur décimale.

.. exercice::

	#.	Combien de nombres peut-on encoder sur 4 bits ?
	#.	Donne l'écriture binaire du plus petit nombre et celle du plus grand nombre.
	#.	Combien de bits sont nécessaires pour encoder le nombre signé :math:`-54` ?

.. exercice::

	Donner l'encodage binaire sur 1 octet des nombres signés suivants. On détaillera la méthode.
	
	a.	:math:`-17`
	b.	:math:`33`
	c.	:math:`-78`
	d.	:math:`-104`
   
.. exercice::

	On donne l'encodage binaire :math:`1011010` d'un nombre signé dont on ne connait pas la valeur décimale.

	#.	Ce nombre est-il négatif ou positif ? Justifier.
	#.	Le nombre est encodé sur 7 bits. Donner un encadrement (décimal) de sa valeur.
	#.	Quelle est la valeur décimale de ce nombre ? Justifier.

.. exercice::
	
	On donne l'encodage binaire de deux nombres signés :math:`1001` et :math:`0100`.

	#.	Quelle est la valeur décimale de chaque nombre ?
	#.	Calculer la somme binaire de ces deux nombres. 
	#.	Calculer la multiplication binaire de ces deux nombres. 
	#.	Vérifier les résultats des 2 opérations en donnant leur écriture décimale.
