Nombre signé en binaire
=======================

Ecriture binaire
----------------

Les nombres entiers négatifs et positifs sont appelés des **nombres signés**.

-   Si l’écriture binaire du nombre commence par :math:`1` (bit de poids fort), alors le nombre est **négatif**.
-   Si l’écriture binaire du nombre commence par :math:`0` (bit de poids fort), alors le nombre est **positif**.

Sur un octet (8 bits), on peut encoder :math:`2^{8}=256` nombres entiers distincts. La moitié de ces nombres sont négatifs et l'autre moitié sont des nombres positifs. 

-   Sur 1 octet, on a 128 nombres négatifs compris entre :math:`-2^{7}=-128` et :math:`-1`,
-   Sur 1 octet, on a 128 nombres positifs compris entre :math:`0` et :math:`2^{7}-1=127`.

Nombre de bits
---------------

Le nombre minimum de bits pour encoder un nombre signé en binaire dépend de la valeur de ce nombre. Pour connaître le nombre minimum de bits, il suffit de trouver le plus petit intervalle dont les bornes sont deux puissances de 2 contenant le nombre signé à encoder.

.. admonition:: Exemple

	Combien de bits pour écrire le nombre :math:`-23` en binaire ?

	On a :math:`-32 \leqslant -23 <32` ce qui est équivalent à :math:`-2^{5} <	-23 < 2^{5}`.

	On en déduit qu'il faut au minimum :math:`5+1=6` bits pour encoder en binaire le nombre :math:`-23`.

Encoder un nombre signé
------------------------

Si le nombre signé est positif, on l'encode en utilisant la méthode vue en numération. On s'assure que le bit de poids fort est :math:`0`.

.. admonition:: Exemple
    
    Le nombre positif :math:`25` se décompose en base :math:`2` par :math:`16+8+1=2^{4}+2^{3}+2^{0}` ce qui nécessite au moins 6 bits pour l'encoder. 
    
    On a donc :math:`25_{10} = 011001_{2}`.
    
    
Si le nombre signé est négatif, on utilise la méthode du **complément à 2** pour l'encoder en binaire. Elle se fait en trois étapes:

#.  Écriture binaire de la valeur absolue du nombre négatif,
#.  Écriture du **complément à 1** de cette écriture binaire, ce qui signifie que chaque bit 0 est remplacé par le bit 1 et que chaque bit 1 est remplacé par le bit 0.
#.  Écriture du **complément à 2**, c’est à dire en ajoutant 1 au complément à 1.

.. admonition:: Exemple

    Le nombre négatif :math:`-25` se code en binaire sur 6 bits.

    -   On écrit le nombre :math:`25` en binaire sur 6 bits, soit :math:`011001`.
    -   Ensuite, le complément à 1 de :math:`011001` est :math:`100110`. On a remplacé chaque bit 0 par 1 et chaque bit 1 par 0.
    -   Enfin, le complément à 2 de :math:`100110` est :math:`100110+1=100111`. On a ajouté :math:`1` au complément à 1.

    Le nombre :math:`-25` est encodé en binaire par :math:`100111` sur 6 bits.

