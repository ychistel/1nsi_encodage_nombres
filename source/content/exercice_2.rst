Exercices
=========

Pour les exercices on donne le tableau de puissances de 2 suivant:

.. csv-table::
   :file: ../csv/puissances_deux.csv
   :delim: ;
   :class: border-style-solid border-width-1

.. exercice::
    
    #.	Donner la valeur en écriture décimale des nombres binaires à virgule fixe suivants:
    
        a.	:math:`111,1`
        b.	:math:`10000000,00000001`
        c.	:math:`0,111`
        
    #.	Donner l'écriture en virgule flottante des nombres précédents.

.. exercice::

    On représente les nombres à virgule avec une virgule fixe. On alloue 2 octets pour le nombre placé avant la virgule et un octet pour la partie avant la virgule.

    #.  Si la partie avant la virgule est un nombre entier positif, quelle est la plus grande valeur que l'on peut représenter en binaire ?
    #.  Quelle est la plus grande valeur que l'on peut représenter en binaire pour la partie située après la virgule ?
    #.  Quel est le plus grand nombre que l'on peut obtenir ?
    #.  Quel est le plus grand nombre que l'on peut représenter si la valeur avant la virgule est un nombre signé.
    
.. exercice::

    On donne une méthode pour convertir un nombre en écriture décimale en écriture binaire avec une virgule fixe.

    -   On convertit la partie entière (avant la virgule) comme la conversion d'un entier (non signé).
    -   Pour la partie décimale (après la virgule), on applique l'algorithme suivant::
        
        tant que la partie décimale est différente de 0:

        -   on multiplie par 2 la partie décimale;
        -   le chiffre avant la virgule est la valeur du bit cherché;
        -   on recommence avec la partie décimale du résultat calculé précédemment.

    On donne un exemple pour convertir :math:`2,625` en binaire.

    Le nombre :math:`2` se convertit :math:`10` en binaire. Appliquons l'algorithme pour :math:`0,625`

    -   :math:`0,625 \times 2 = 1,25` donc le premier bit après la virgule vaut 1; on recommence avec :math:`0,25`
    -   :math:`0,25\times 2 = 0,5` donc le deuxième bit après la virgule vaut 0; on recommence avec :math:`0.5`
    -   :math:`0,5 \times 2 = 1,0` donc le troisième bit après la virgule vaut 1; la partie décimale est 0, on arête.

    Conclusion : :math:`2,625_{10} = 10,101_{2}`

    #.  Donner l'écriture binaire du nombre :math:`5,6875_{10}`.
    #.  Donner l'écriture binaire du nombre :math:`13,15625_{10}`.
    #.  Donner l'écriture binaire du nombre :math:`0,2_{10}`. Que remarque-t-on ?

.. exercice::

    On donne le nombre dont la représentation binaire suit la norme IEEE 754.

    .. figure:: ../img/flottant_3.svg
        :align: center
        :width: 480

    Quelle est la valeur décimale de ce nombre ?

